from random import SystemRandom
from string import ascii_letters, digits
from django.db import models

SLUG_LENGTH = 8


class Slug(models.Model):
    """
    Database table that will store our slugs
    and related helper functions.
    """
    site_title = models.CharField(max_length=200)
    target_url = models.CharField(max_length=200, unique=True, db_index=True)
    slug = models.CharField(max_length=SLUG_LENGTH, unique=True, db_index=True)
    create_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    hit_count = models.IntegerField(default=0)
    creator_ip = models.GenericIPAddressField()

    def __str__(self):
        """
        Giving this model a descriptive string representation.
        :return: String descriptor
        """
        return "{} - {}".format(self.site_title, self.slug)

    def save(self, *args, **kwargs):
        """
        Helper function to save the models to the database.
        """
        self.slug = self.slug or self.new_slug()
        super(Slug, self).save(*args, **kwargs)

    def new_slug(self):
        """
        Helper function to get a new slug if one is not provided.
        :return: a unique, random slug of length SLUG_LENGTH
        """
        slug = self._generate_random_slug()
        if Slug.objects.filter(slug=slug).count() > 0:
            slug = self.new_slug()
        return slug

    def get_dict(self):
        """
        Helper function to return a dictionary of all values
        associated with a model.
        :return: Dict object
        """
        payload = {}
        payload['site_title'] = self.site_title
        payload['target_url'] = self.target_url
        payload['slug'] = self.slug
        payload['create_date'] = self.create_date.isoformat()
        payload['hit_count'] = self.hit_count
        return payload

    def _generate_random_slug(self):
        """
        Helper function to generate a random slug of length SLUG_LENGTH.
        :return: random slug of length SLUG_LENGTH
        """
        size = SLUG_LENGTH
        chars = ascii_letters + digits
        return ''.join(SystemRandom().choice(chars) for _ in range(size))
