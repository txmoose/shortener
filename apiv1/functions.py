"""
Helper functions for views that should be moved out of
the views.py file
"""
from django.db import IntegrityError
from django.shortcuts import get_object_or_404

from .models import Slug, SLUG_LENGTH

from lxml.html import fromstring
import requests
import json


def scrape_title(target_url):
    """
    Helper Function to scrape site titles
    :return: string - title of target_url
    """
    # Scraping site title from site
    r = requests.get(target_url)
    site_title = fromstring(r.content).findtext('.//title')
    # If no title element, use domain as site_title
    if not site_title:
        site_title = target_url.split("//")[-1].split("/")[0]

    return site_title


def get_slug_json(slug):
    """
    Helper function to get a slug from a database and
    return a JSON object containing it
    :slug: string of a valid slug
    :return: json object
    """
    response = {}
    response['target_url'] = get_object_or_404(Slug,
                                               slug__exact=slug).target_url
    return json.dumps(response)


def get_detail_json(slug):
    """
    Helper function to get details from a database and
    return a JSON object containing it
    :slug: string of a valid slug
    :return: json object
    """
    response = {}
    response = get_object_or_404(Slug, slug__exact=slug).get_dict()
    return json.dumps(response)


def create_new_slug(request, body):
    """
    Helper function that sits behind all differing
    create views for unity sake
    :request: request object
    :body: dictionary object
    """
    try:
        new_url = Slug()

        # If slug is provided in JSON:
        if 'slug' in body.keys() and len(body['slug']) <= SLUG_LENGTH:
            # Check that slug doesn't already exist
            if Slug.objects.filter(slug=body['slug']).count() > 0:
                response = {}
                response['slug'] = body['slug']
                response['created'] = False
                response['reason'] = '{} already exists'.format(body['slug'])

                return response

            # If slug doesn't exist already
            else:
                new_url.slug = body['slug']

        # If slug is NOT provided in JSON, generate one
        else:
            new_url.slug = new_url.new_slug()

        # Getting base information from request JSON
        new_url.target_url = body.get('target_url')
        new_url.creator_ip = request.META.get('REMOTE_ADDR')
        new_url.site_title = scrape_title(body['target_url'])

        new_url.save()

        # New record created, set created to True
        response = new_url.get_dict()
        response['created'] = True

    # If target_url is already in database, return that record instead
    except IntegrityError:
        new_url = Slug.objects.get(target_url=body['target_url'])
        response = new_url.get_dict()

        # New record NOT created, set created to False
        response['created'] = False

    return response
