from django.contrib import admin

# Register your models here.

from .models import Slug


class SlugAdmin(admin.ModelAdmin):
    list_display = ('site_title',
                    'target_url',
                    'slug',
                    'hit_count',
                    'create_date',
                    'creator_ip')


admin.site.register(Slug, SlugAdmin)
