from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.defaults import bad_request
from django.http import HttpResponse

from .functions import create_new_slug, get_slug_json, get_detail_json

import json


def slug(request, slug):
    """
    View that reuturns a target_url for a given slug
    :return: HttpResponse JSON object
    """
    return HttpResponse(get_slug_json(slug))


def detail(request, slug):
    """
    View that returns a JSON of the full information
    of a given slug.
    :return: HttpResponse JSON object
    """
    return HttpResponse(get_detail_json(slug))


# Need CSRF exempt decorator to allow requests without CSRF tokens
@require_POST
@csrf_exempt
def create(request):
    """
    View that will create a new slug and return new slug
    :return: HttpResponse JSON object
    """
    # Try/Except to catch malformed request bodies elegantly
    try:
        body = json.loads(request.body.decode())
        if 'target_url' not in body.keys():
            raise Exception('Body was not valid JSON')

        response = create_new_slug(request, body)

    except Exception as e:
        return bad_request(request, e)

    if response.get('created'):
        return HttpResponse(json.dumps(response))
    else:
        return HttpResponse(json.dumps(response), status=409)
