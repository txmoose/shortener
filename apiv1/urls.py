from django.conf.urls import url

from . import views
from .models import SLUG_LENGTH

SLUG_REGEX = "[a-zA-Z0-9]{{1,{0}}}".format(SLUG_LENGTH)

app_name = 'apiv1'
urlpatterns = [
    url(r'^(?P<slug>{})$'.format(SLUG_REGEX), views.slug, name='slug'),
    url(r'^(?P<slug>{})/detail/$'.format(SLUG_REGEX), views.detail, name='detail'),
    url(r'^create/$', views.create, name='create'),
]
