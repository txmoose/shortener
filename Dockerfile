FROM python:3-onbuild
WORKDIR /usr/src/app
COPY . /usr/src/app
EXPOSE 8001
CMD ["uwsgi", "--ini", "uwsgi.ini"]
