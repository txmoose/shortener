from django import forms


class StandardCreateForm(forms.Form):
    target_url = forms.URLField(label='target url')


class CustomCreateForm(forms.Form):
    target_url = forms.URLField(label='target url')
    slug = forms.CharField(label='slug', min_length=2, max_length=8,
                           required=True, strip=True)
