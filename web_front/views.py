from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponseRedirect

from apiv1.models import Slug
from apiv1.functions import create_new_slug

from .forms import StandardCreateForm, CustomCreateForm


def slug(request, slug):
    """
    Receives slug from URI, returns 301 redirect to
    target URL to browser.  If slug does not exist,
    returns 404
    :return: redirect to target URL or 404
    """
    target_url = get_object_or_404(Slug, slug__exact=slug)
    target_url.hit_count += 1
    target_url.save()
    return redirect(target_url.target_url)


def index(request):
    """
    Landing page for web front.  Will be a form with a
    text box and submit button to create a randomly-generated
    slug.
    """
    if request.method == 'POST':
        # Create a form instance and populate it with data from the request
        form = StandardCreateForm(request.POST)
        if form.is_valid():
            body = {"target_url": form.cleaned_data.get("target_url")}
            response = create_new_slug(request, body)
            return HttpResponseRedirect('/{}/detail/'.format(response.get('slug')))

    # If a GET, create a blank form
    else:
        form = StandardCreateForm()

    return render(request, 'web_front/create.html', {'form': form})


def custom(request):
    """
    Form with 2 text boxes and a submit button.  Users can
    create a customized slug here.
    """
    if request.method == 'POST':
        # Create a form instance and populate it with data from the request
        form = CustomCreateForm(request.POST)
        if form.is_valid():
            body = {"target_url": form.cleaned_data.get("target_url"),
                    "slug": form.cleaned_data.get("slug")}
            print(body)
            response = create_new_slug(request, body)
            return HttpResponseRedirect('/{}/detail/'.format(response.get('slug')))

    # If a GET, create a blank form
    else:
        form = CustomCreateForm()

    return render(request, 'web_front/create.html', {'form': form})


def detail(request, slug):
    """
    Page where users can see detailed information about a
    slug before visiting the target_url.
    """
    slug_obj = get_object_or_404(Slug, slug__exact=slug)
    return render(request, 'web_front/detail.html', {'slug': slug_obj})


def recent(request):
    """
    Page where users can see the X most recently created
    slugs and target URLs.
    """
    slug_list = Slug.objects.order_by('-create_date')[:5]
    return render(request, 'web_front/recent.html', {'slug_list': slug_list, })
