from django.views.generic import RedirectView
from django.conf.urls import url

from . import views
from apiv1.urls import SLUG_REGEX


app_name = 'web_front'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^custom/$', views.custom, name='custom'),
    url(r'^recent$', RedirectView.as_view(url='recent/')),
    url(r'^recent/$', views.recent, name='recent'),
    url(r'^(?P<slug>{})/?$'.format(SLUG_REGEX), views.slug, name='slug'),
    url(r'^(?P<slug>{})/detail/$'.format(SLUG_REGEX), views.detail, name='detail'),
]
