from django.apps import AppConfig


class WebFrontConfig(AppConfig):
    name = 'web_front'
