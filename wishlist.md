### Stuff I want

#### Hardening
* Rate limit incoming requests based on IP
    * App, server, or iptables level?
* Input validation
    * For URLs, must begin http(s)?://
    * For slugs, must be alphanumeric
* Only allow GET/POST request types to correct endpoints

#### Wants/thoughts
* Material Design to scale to phone/tablet/monitor
* How do we test views?
    * Selenium?
* Signups & API Keys/tokens for using API?
* Front end website written in JS to consume API?
* Material Cards for each slug on Recent page
    * Tap card to bring up detail page for slug in card

#### Tests
* Need FULL unit tests
    * failure cases
    * URL validation
        * Use validators to validate string, then use requests to validate http status code
    * Views of front end site
