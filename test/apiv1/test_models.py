from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse

from apiv1.models import Slug
from apiv1.urls import SLUG_REGEX

import re


class TestApiv1(TestCase):
    """
    Test suite for APIv1 App.
    """
    def setUp(self):
        """
        Helper method to set up test cases.
        """
        self.subject = Slug(site_title="Example Domain",
                            target_url="http://example.org",
                            creator_ip="127.0.0.1")

    def test_save_generates_new_slug_if_empty(self):
        self.assertEqual('', self.subject.slug)

        self.subject.save()

        slug_string = self.subject.slug
        self.assertTrue(re.match(SLUG_REGEX + "$", slug_string),
                        "Expected {} to match regex.".format(slug_string))

    def test_save_keeps_existing_slug(self):
        self.subject.save()
        existing_slug = self.subject.slug
        self.subject.save()
        new_slug = self.subject.slug
        self.assertEqual(existing_slug, new_slug)

    def test_new_slug_generates_a_new_slug_that_matches_slug_regex(self):
        slug_string = self.subject.new_slug()
        self.assertTrue(re.match(SLUG_REGEX + "$", slug_string),
                        "Expected {} to match regex.".format(slug_string))

    def test_new_slug_returns_a_randomly_generated_slug_each_time(self):
        slug1 = self.subject.new_slug()
        slug2 = self.subject.new_slug()
        self.assertNotEqual(slug1, slug2,
                            "Expected {0} and {1} to not match.".format(slug1, slug2))

    def test_new_slug_ensures_slug_uniqueness(self):
        self.subject.slug = 'ABCD1234'
        self.subject.save()

        with patch.object(self.subject, '_generate_random_slug') as patched_generate_slug:
            patched_generate_slug.side_effect = ['ABCD1234', 'EFGH5678']
            self.assertEqual(self.subject.new_slug(), 'EFGH5678')

    def test_slug_is_case_sensitive(self):
        self.subject.save()
        slug = self.subject.slug.swapcase()
        slug2 = Slug(site_title="Example Domain 2",
                     target_url="http://example.com",
                     creator_ip="127.0.0.1",
                     slug=slug)
        slug2.save()
        test_url = Slug.objects.get(slug=slug)
        self.assertNotEqual(test_url.target_url, "http://example.org")

    def test_slug_view_returns_expected_json(self):
        self.subject.save()
        slug = self.subject.slug
        response = self.client.get(reverse('apiv1:slug',
                                           kwargs={'slug': slug}))
        self.assertEqual(response.content,
                         b'{"target_url": "http://example.org"}')
